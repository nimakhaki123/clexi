if (document.querySelector(".close__item")) {
  document.querySelector(".mobile__menu").addEventListener("click", () => {
    document
      .querySelector("header .mobile__menu__items")
      .classList.toggle("d-none");
  });
  document.querySelector(".close__item ").addEventListener("click", () => {
    document
      .querySelector("header .mobile__menu__items")
      .classList.toggle("d-none");
  });
}

document.querySelectorAll("video").forEach((video) => {
  video.addEventListener("click", (item) => {
    item.currentTarget.setAttribute("controls", "controls");
    item.currentTarget.play();
  });
});

if (document.querySelector(".gallery .item img")) {
  function openImageBox(img) {
    document
      .querySelector(".overlay__gallery")
      .classList.add("overlay__gallery__active");
    document.getElementById("image__box__pic").src = img;
    document.querySelector(".image__box").classList.remove("d-none");
    document.querySelector("body").classList.add("overflow-hidden");
  }
  function closeImageBox() {
    document
      .querySelector(".overlay__gallery")
      .classList.remove("overlay__gallery__active");

    document.querySelector(".image__box").classList.add("d-none");
    document.querySelector("body").classList.remove("overflow-hidden");
  }
  document.querySelectorAll(".gallery .item img").forEach((item) => {
    item.addEventListener("click", (element) => {
      openImageBox(element.currentTarget.src);
    });
  });
}

document.querySelectorAll(".has__sub").forEach((element) => {
  element.addEventListener("click", (item) => {
    item.currentTarget.classList.toggle("open__sub");
  });
});
